/**
$(document).ready(function(){
    console.log("Hello World!");
});

General Syntax of jQuery:
$(selector).member_to_be_accessed
So in the above code we accessed 'document' element and created a function for its ready event!
The same can be achieved with the help of the shorthand version of the document.ready which is as follows:
$(function(){
    //body
})
**/

$(function(){
    $(".top-bar").fadeIn(2000, function(){
        alert("called");
    });
    
    $("h1").css("color","blue");
    
    $(".play-with-box").click(function(){
        $("#box").fadeOut(1000);
    });
    
    $("#hideMe").click(function(){
        $(".top-bar").fadeOut(1000);
    });


/******************************************
            jQUERY SELECTORS
******************************************/
//GROUPING SELECTORS
//$("h1, h2, h3, h4").css("border","solid 1px red");

//ID SELECTORS
//$("div#container").css("border","solid 1px red");

//CLASS SELECTOR
//    $("p.lead").css("border","solid 1px red");
    
//    PSEUDO ELEMENT SELECTOR THAT CONTAINS FIRST CHILD!
//    $("li:first").css("border","solid 1px red");
    
//    PSEUDO ELEMENT SELECTOR THAT CONTAINS ALL EVEN CHILD!
//    $("li:even").css("border","solid 1px red");

//    DESCENDENT SELECTOR
//    $("div em").css("border","solid 1px red");
    
//    CHILD SELECTOR
//    $("div>p").css("border","solid 1px red");
    
//    jQUERY SELECTOR TO SELECT ALL HEADING TAGS(h1 to h6)
//    $(":header").css("border","solid 1px red");

//    jQUERY CONTAINS SELECTOR
//    $("div:contains('love')").css("border","solid 1px red");
    
    
    /***********************************
                jQUERY EVENTS
    ***********************************/
    $("#box").click(function(){
        alert("You just clicked that box!");
    });
    
    $("input[type=text]").blur(function(){
        if($(this).val() == ""){
            $(this).css("border","solid 1px red");
            $("#box").text("You forgot to add something!");
        }
    });
    
    $("input[type=text]").keydown(function(){
        if($(this).val() != ""){
            $(this).css("border","solid 1px green");
            $("#box").text("Thanks for that!");
        }
    });
    
    $("#box").hover(function(){
        $(this).text("You hovered In!");
    }, function(){
        $(this).text("You hovered Out!")
    });
    
    
    /*****************************************
                jQUERY CHAINING
    *****************************************/
    /*
    Here all the effects after the dot are all applied to the element written in $("")
    That is delay slidedown and slideup all are applied on (".notification-bar")
    */
    
    $(".notification-bar").delay(2000).slideDown().delay(2000).slideUp();
    
    /*****************************************
                jQUERY HIDE/SHOW
    *****************************************/
//    $(".hidden").show();
//    $("h1").hide();
//    $(".hidden").fadeIn(8000);
    $("#box1").click(function(){
        $(this).fadeTo(1000,0.25, function(){
            $(this).slideUp();
        });
    });
    
    $("div.hidden").slideDown();
    
    $("button").click(function(){
        $("#box1").slideToggle();
    });
    
    /*****************************************
                jQUERY Animate
    *****************************************/
    $("#left").click(function(){
        $(".box").animate({
            left: "-=40px",
            fontSize: "+=2px"
        })
    });
    
        $("#right").click(function(){
        $(".box").animate({
            left: "+=40px",
            fontSize: "-=2px"
        })
    });
        $("#up").click(function(){
        $(".box").animate({
            top: "-=40px",
            opacity: "+=0.1"
        })
    });
        $("#down").click(function(){
        $(".box").animate({
            top: "+=40px",
            opacity: "-=0.1"
        })
    });
    
    
    /*****************************************
                jQUERY CSS
    *****************************************/
    $("#circle2").css({
        "background":"#8a8",
        "color":"#fff",
        "width":"150px",
        "height":"150px",
        "line-height":"150px",
        "margin":"40px",
        "display":"inline-block"
    }).addClass("circleShape");
    
    
    $("#name").blur(function(){
        if($(this).val() == ""){
            $(this).addClass("danger");
            $(this).removeClass("success");
        }
        else{
            $(this).addClass("success");
            $(this).removeClass("danger");
        }
    });
    
    
    /******************************
            CAR GAME
    *******************************/
    $("#go").click(function(){
        function checkIfComplete(){
            if(isComplete == false)
                isComplete = true;
            else
                place = "Second";
        }
    
        var place = "First";
        var isComplete = false;
        
        var carTime1  = Math.floor(1 + Math.random() * 5000);        
        var carTime2  = Math.floor(1 + Math.random() * 5000);        
        
        var WindowWidth = $(window).width();
        
        var car1Width = $("#car1").width();
        var raceTrackWidt1 = WindowWidth - car1Width;
                  
        var car2Width = $("#car2").width();
        var raceTrackWidt2 = WindowWidth - car2Width;
        
        $("#car1").animate({
            left : raceTrackWidt1
        }, carTime1, function(){
            checkIfComplete();
            $("#raceInfo1 span").text("Finished in "+ place + "place and clocked in at "+ carTime1 + "ms");
        });
        
        $("#car2").animate({
            left : raceTrackWidt2
        }, carTime2, function(){
            checkIfComplete();
            $("#raceInfo2 span").text("Finished in "+ place + "place and clocked in at "+ carTime2 + "ms");
        });
    });
    
    $("#reset").click(function(){
        window.location.reload();
    })
});



















